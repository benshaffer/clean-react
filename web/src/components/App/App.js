import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { StoreProvider } from '../_common';
import { useStore } from '../../hooks';
import Navigation from '../Navigation';
import Content from '../Content';
import './App.scss';

const App = () => {
    const store = useStore();

    return (
        <StoreProvider store={store}>
            <BrowserRouter>
                <div className="App">
                    <Navigation />
                    <Content />
                </div>
            </BrowserRouter>
        </StoreProvider>
    );
};

export default App;
