import React from 'react';
import PropTypes from 'prop-types';
import Card from '@material-ui/core/Card';
import './WeatherItem.scss';

const WeatherItem = ({ weather }) => (
    <Card className="WeatherItem-Container">
        <span className="WeatherItem-Header">
            <img className="WeatherItem-Image" src={weather.icon} alt="Weather Icon" />
            <h4>{weather.name}</h4>
        </span>
        <div>{weather.isDaytime}</div>
        <div>
            <b>Temperature:</b> {weather.temperature} &deg;{weather.temperatureUnit}
        </div>
        <div>
            <b>Wind Speeds:</b> {weather.windSpeed}, {weather.windDirection}
        </div>
        <br />
        <div>{weather.detailedForecast}</div>
    </Card>
);

WeatherItem.propTypes = {
    weather: PropTypes.shape({
        id: PropTypes.string,
        name: PropTypes.string,
        isDaytime: PropTypes.bool,
        temperature: PropTypes.number,
        temperatureUnit: PropTypes.string,
        windSpeed: PropTypes.string,
        windDirection: PropTypes.string,
        icon: PropTypes.string,
        shortForecast: PropTypes.string,
        detailedForecast: PropTypes.string,
    }),
};

export default WeatherItem;
