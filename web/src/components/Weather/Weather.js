import React from 'react';
import { useWeather } from '../../hooks';
import WeatherItem from './WeatherItem';
import './Weather.scss';

const Weather = () => {
    const [{ forecast }] = useWeather();

    return (
        <div className="Weather-Container">
            {forecast.map(weather => (
                <WeatherItem key={weather.id} weather={weather} />
            ))}
        </div>
    );
};

export default Weather;
