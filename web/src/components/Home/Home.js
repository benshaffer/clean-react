import React from 'react';
import './Home.scss';

const Home = () => {
    return <div>This is the Home page and should contain a description of the application.</div>;
};

export default Home;
