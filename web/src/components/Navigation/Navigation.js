import React from 'react';
import { Link } from 'react-router-dom';
import Routes from '../../common/Routes';
import './Navigation.scss';

const Navigation = () => (
    <div className="Navigation">
        <Link className="Navigation-item" to={Routes.Todo}>
            todo
        </Link>
        <Link className="Navigation-item" to={Routes.Weather}>
            weather
        </Link>
    </div>
);

export default Navigation;
