import React from 'react';
import PropTypes from 'prop-types';
import Checkbox from '@material-ui/core/Checkbox';
import Paper from '@material-ui/core/Paper';
import './TodoItem.scss';

const TodoItem = ({ todo, updateTodo }) => (
    <Paper className="TodoItem-Container" onClick={() => updateTodo({ ...todo, isCompleted: !todo.isCompleted })}>
        <span className="TodoItem-Checkbox">
            <Checkbox
                checked={todo.isCompleted}
                color="primary"
                inputProps={{
                    'aria-label': 'checkbox',
                }}
            />
        </span>

        <span className="TodoItem-Label">{todo.title}</span>
    </Paper>
);

TodoItem.propTypes = {
    todo: PropTypes.shape({
        id: PropTypes.string,
        title: PropTypes.string,
        isCompleted: PropTypes.bool,
    }),
    updateTodo: PropTypes.func,
};

export default TodoItem;
