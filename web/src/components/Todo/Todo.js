import React from 'react';
import { useTodos } from '../../hooks';
import Button from '@material-ui/core/Button';
import Grid from '@material-ui/core/Grid';
import Icon from '@material-ui/core/Icon';
import TextField from '@material-ui/core/TextField';
import TodoItem from './TodoItem';
import './Todo.scss';

const Todo = () => {
    const [todos, { addTodo, updateTodo }] = useTodos();
    const inputRef = React.useRef('');

    return (
        <div className="Todo-Container">
            <div className="Todo-List">
                <div className="Todo-Form">
                    <TextField className="Todo-Input" placeholder="Add Item" inputRef={inputRef} />
                    <Button className="Todo-Button" onClick={() => addTodo(inputRef.current.value)}>
                        <Icon color="primary" fontSize="large">
                            add_circle
                        </Icon>
                    </Button>
                </div>
                <Grid className="Todo-List-Items" container spacing={1}>
                    {todos.length > 0 &&
                        todos.map(todo => {
                            return (
                                <Grid className="Todo-List-Item" item key={todo.id}>
                                    <TodoItem todo={todo} updateTodo={updateTodo} />
                                </Grid>
                            );
                        })}
                </Grid>
            </div>
        </div>
    );
};

export default Todo;
