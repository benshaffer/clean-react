import React from 'react';

const StoreContext = React.createContext({});

const StoreProvider = props => {
    return <StoreContext.Provider value={props.store}>{props.children}</StoreContext.Provider>;
};

export { StoreContext, StoreProvider };
