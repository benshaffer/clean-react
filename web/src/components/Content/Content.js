import React from 'react';
import { Switch, Route } from 'react-router-dom';
import Routes from '../../common/Routes';
import Home from '../Home';
import Todo from '../Todo';
import Weather from '../Weather';
import './Content.scss';

const Content = () => (
    <div className="Content">
        <Switch>
            <Route path={Routes.Home} exact component={Home} />
            <Route path={Routes.Todo} component={Todo} />
            <Route path={Routes.Weather} component={Weather} />
        </Switch>
    </div>
);

export default Content;
