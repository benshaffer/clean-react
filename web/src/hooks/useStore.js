import { useReducer } from 'react';
import { setupService } from 'clean-react-core';

const initialState = setupService.getInitialState();
const rootReducer = setupService.getRootReducer();

export const useStore = () => useReducer(rootReducer, initialState);
