import { useEffect } from 'react';
import { weatherService } from 'clean-react-core';
import { useStoreContext } from '.';

export const useWeather = () => {
    const [{ weather: forecast }, dispatch] = useStoreContext();

    useEffect(() => {
        const fetch = async () => {
            const result = await weatherService.getWeatherForCurrentLocation();

            if (!result.error) {
                dispatch(result);
            }
        };

        fetch();
    }, []);

    return [forecast];
};
