import { todoService } from 'clean-react-core';
import { useStoreContext } from '.';

export const useTodos = () => {
    const [{ todo }, dispatch] = useStoreContext();

    const addTodo = title => {
        const result = todoService.addTodo(title);

        if (!result.error) {
            dispatch(result);
        }
    };

    const updateTodo = todo => {
        const result = todoService.updateTodo(todo);

        if (!result.error) {
            dispatch(result);
        }
    };

    return [todo.todos, { addTodo, updateTodo }];
};
