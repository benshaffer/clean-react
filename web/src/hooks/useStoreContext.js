import { useContext } from 'react';
import { StoreContext } from '../components/_common/StoreProvider';

export const useStoreContext = () => useContext(StoreContext);
