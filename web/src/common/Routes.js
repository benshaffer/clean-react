const Routes = {
    Home: '/',
    Todo: '/todo',
    Weather: '/weather',
};

export default Routes;
