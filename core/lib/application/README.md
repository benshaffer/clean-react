# Application Layer

The application layer should contain all code that reflects application business logic:

- Validation Logic
- Interfaces
- Internal Services

Application layer should only import from the domain layer.
