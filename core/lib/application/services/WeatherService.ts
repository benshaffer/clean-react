import { Action } from "..";
import { IHttpClient } from "../interfaces/services/IHttpClient";
import { IWeatherActionService } from "../interfaces/services/IWeatherActionService";
import { ActionError } from "../interfaces";

export class WeatherService {
  private httpClient: IHttpClient;
  private weatherActions: IWeatherActionService;

  constructor(httpClient: IHttpClient, weatherActions: IWeatherActionService) {
    this.httpClient = httpClient;
    this.weatherActions = weatherActions;
  }

  public getWeatherForCurrentLocation = async (): Promise<Action> => {
    let forecast = null;

    try {
      const result = await this.httpClient.get(
        "https://api.weather.gov/gridpoints/TOP/31,80/forecast"
      );

      forecast = result!.data!.properties!.periods;

      if (!forecast) {
        throw new Error("Failed to retrieve weather data.");
      }

      return this.weatherActions.setCurrentWeather(forecast);
    } catch (e) {
      const error = new ActionError(e.message, e.stack);
      return this.weatherActions.setCurrentWeather(forecast, error);
    }
  };
}
