import { Action } from "..";
import { ITodoActionService } from "../interfaces/services/ITodoActionService";
import { ActionError } from "../interfaces";
import { Todo } from "../../domain";

export class TodoService {
  private todoActions: ITodoActionService;

  constructor(todoActions: ITodoActionService) {
    this.todoActions = todoActions;
  }

  public addTodo = (title: string): Action => {
    try {
      if (!title) {
        throw new Error("You must enter a title to create a Todo.");
      }

      return this.todoActions.addTodo(title);
    } catch (e) {
      const error = new ActionError(e.message, e.stack);
      return this.todoActions.addTodo(title, error);
    }
  };

  public updateTodo = (todo: Todo): Action => {
    try {
      if (!todo || !todo.id || !todo.title) {
        throw new Error("Todo must contain an id and a title.");
      }

      return this.todoActions.updateTodo(todo);
    } catch (e) {
      const error = new ActionError(e.message, e.stack);
      return this.todoActions.updateTodo(todo, error);
    }
  };
}
