import { IStateService } from "../interfaces";

export class SetupService {
  private stateService: IStateService;

  public constructor(stateService: IStateService) {
    this.stateService = stateService;
  }

  public getInitialState = () => {
    return this.stateService.getInitialState();
  };

  public getRootReducer = () => {
    return this.stateService.getRootReducer();
  };
}
