import { TodoState } from ".";
import { WeatherState } from ".";

export interface State {
  todo: TodoState;
  weather: WeatherState;
}
