import { Todo } from "../../../domain";

export interface TodoState {
  todos: Todo[];
}
