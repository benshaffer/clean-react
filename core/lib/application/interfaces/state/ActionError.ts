export class ActionError {
  readonly message: string;
  readonly stackTrace: string;

  constructor(message: string, stackTrace: string) {
    this.message = message;
    this.stackTrace = stackTrace;

    console.debug(
      `Application Error:
      \nStack Trace: ${this.stackTrace}`
    );
  }
}
