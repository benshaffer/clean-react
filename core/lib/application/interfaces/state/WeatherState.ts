import { Weather } from "../../../domain";

export interface WeatherState {
  forecast: Weather[];
}
