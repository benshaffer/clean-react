import { Action, ActionError } from "..";
import { Weather } from "../../../domain";

export interface IWeatherActionService {
  setCurrentWeather: (data: Weather[], error?: ActionError) => Action;
}
