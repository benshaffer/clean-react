import { Action, ActionError } from "..";
import { Todo } from "../../../domain";

export interface ITodoActionService {
  addTodo: (title: string, error?: ActionError) => Action;
  updateTodo: (todo: Todo, error?: ActionError) => Action;
}
