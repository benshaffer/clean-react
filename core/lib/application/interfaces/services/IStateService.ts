import { State } from "../state/State";

export interface IStateService {
  getRootReducer: () => any;
  getInitialState: () => State;
}
