export interface IHttpClient {
  get: (url: string) => Promise<any>;
  delete: () => Promise<any>;
  patch: () => Promise<any>;
  post: () => Promise<any>;
  put: () => Promise<any>;
}
