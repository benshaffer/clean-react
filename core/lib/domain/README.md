## Domain Layer

The domain layer should contain all code that reflects enterprise logic:

- Entities
- Enums

Domain layer should never import from another layer.
