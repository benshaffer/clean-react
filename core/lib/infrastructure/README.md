## Infrastructure Layer

The infrastructure layer should contain all code that reflects external logic:

- HTTP Clients
- Authentication
- Analytics Connectors

Infrastructure layer should only import from the application layer.
