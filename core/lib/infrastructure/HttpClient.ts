import axios, { AxiosInstance, AxiosRequestConfig } from "axios";
import { IHttpClient } from "../application";

export class HttpClient implements IHttpClient {
  private client: AxiosInstance;
  private config: AxiosRequestConfig = {};

  constructor() {
    this.client = axios.create(this.config);
  }

  public get = async (url: string): Promise<any> => {
    return await this.client.get(url);
  };

  public delete = (): any => {
    throw new Error("HTTP Delete not yet implemented");
  };

  public patch = (): any => {
    throw new Error("HTTP Patch not yet implemented");
  };

  public post = (): any => {
    throw new Error("HTTP Post not yet implemented");
  };

  public put = (): any => {
    throw new Error("HTTP Put not yet implemented");
  };
}
