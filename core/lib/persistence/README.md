## Persistence Layer

The persistence layer should contain all code that reflects how data is stored in the application:

- Global State Management
- Interaction with Local/Session Storage

Persistence layer should only import from the application layer.
