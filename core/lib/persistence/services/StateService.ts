import { IStateService, State } from "../../application";
import {
  initialTodoState,
  initialWeatherState,
  todoReducer,
  weatherReducer
} from "../store";

export class StateService implements IStateService {
  public getInitialState = (): State => ({
    todo: initialTodoState,
    weather: initialWeatherState
  });

  public getRootReducer = (): any =>
    this.combineReducers({
      todo: todoReducer,
      weather: weatherReducer
    });

  /**
   * A size-optimized refactor of Redux's combineReducers.
   * All safeguards removed. Use at your own risk.
   * https://github.com/reduxjs/redux/blob/master/src/combineReducers.js
   */
  private combineReducers = (reducers: any): any => (
    state: any,
    action: any
  ): any => {
    var hasChanged = false;
    const initialState = {};

    const newState = Object.keys(reducers).reduce(
      (result: any, key: any): any => {
        result[key] = reducers[key](state[key], action);
        hasChanged = hasChanged || result[key] !== state[key];
        return result;
      },
      initialState
    );

    return hasChanged ? newState : state;
  };
}
