import * as uuid from "uuid/v4";
import { WeatherState, Action } from "../../../application";
import { WeatherActions } from ".";

export const initialWeatherState: WeatherState = {
  forecast: []
};

const setCurrentWeather = (data: any[], state: WeatherState): WeatherState => {
  var newState = {
    ...state,
    forecast: data.map(e => ({ id: uuid(), ...e }))
  };

  return newState;
};

export const weatherReducer = (
  state: WeatherState,
  action: Action
): WeatherState => {
  var newState = { ...state };

  switch (action.type) {
    case WeatherActions.SET_CURRENT_WEATHER:
      newState = setCurrentWeather(action.payload, state);
      break;
  }

  return newState;
};
