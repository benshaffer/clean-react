import {
  IWeatherActionService,
  Action,
  ActionError
} from "../../../application";

export enum WeatherActions {
  SET_CURRENT_WEATHER = "WEATHER_SET_CURRENT_WEATHER"
}

export class WeatherActionService implements IWeatherActionService {
  public setCurrentWeather = (data: any, error?: ActionError): Action => ({
    error: error,
    payload: data,
    type: WeatherActions.SET_CURRENT_WEATHER
  });
}
