import { Action, ActionError } from "../../../application";
import { ITodoActionService } from "../../../application/interfaces/services/ITodoActionService";

export enum TodoActions {
  ADD_TODO = "TODO_ADD_TODO",
  UPDATE_TODO = "TODO_UPDATE_TODO"
}

export class TodoActionService implements ITodoActionService {
  public addTodo = (title: string, error?: ActionError): Action => ({
    error: error,
    payload: title,
    type: TodoActions.ADD_TODO
  });

  public updateTodo = (todo: any, error?: ActionError): Action => ({
    error: error,
    payload: todo,
    type: TodoActions.UPDATE_TODO
  });
}
