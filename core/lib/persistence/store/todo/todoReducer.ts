import * as uuid from "uuid/v4";
import { TodoState, Action } from "../../../application";
import { TodoActions } from ".";
import { todoService } from "../../..";

export const initialTodoState: TodoState = {
  todos: [
    {
      id: uuid(),
      title: "Prepare for React Chicago presentation",
      isCompleted: false
    },
    {
      id: uuid(),
      title: "Style Todo and Weather components",
      isCompleted: false
    },
    {
      id: uuid(),
      title: "Add tests for all application services",
      isCompleted: false
    },
    {
      id: uuid(),
      title: "Add speaking note comments to key files",
      isCompleted: false
    },
    {
      id: uuid(),
      title: "Fix linter in web project",
      isCompleted: true
    }
  ]
};

const addTodo = (title: String, state: TodoState): TodoState => {
  var newState = {
    ...state,
    todos: state.todos.concat([
      {
        id: uuid(),
        title: title,
        isCompleted: false
      }
    ])
  };

  return newState;
};

const updateTodo = (todo: any, state: TodoState): TodoState => {
  var newState = {
    ...state,
    todos: state.todos.map(e => {
      if (e.id === todo.id) return todo;
      else return e;
    })
  };

  return newState;
};

export const todoReducer = (state: TodoState, action: Action): TodoState => {
  var newState = { ...state };

  switch (action.type) {
    case TodoActions.ADD_TODO:
      newState = addTodo(action.payload, state);
      break;
    case TodoActions.UPDATE_TODO:
      newState = updateTodo(action.payload, state);
      break;
  }

  return newState;
};
