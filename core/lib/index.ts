import { SetupService, TodoService, WeatherService } from "./application";
import {
  StateService,
  TodoActionService,
  WeatherActionService
} from "./persistence";
import { HttpClient } from "./infrastructure";

const httpClient = new HttpClient();

const stateService = new StateService();
const setupService = new SetupService(stateService);

const todoActions = new TodoActionService();
const todoService = new TodoService(todoActions);

const weatherActions = new WeatherActionService();
const weatherService = new WeatherService(httpClient, weatherActions);

export { setupService, todoService, weatherService };
